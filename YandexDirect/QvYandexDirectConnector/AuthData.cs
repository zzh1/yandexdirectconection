﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QvYandexDirectConnector
{
	public static class AuthData
	{
		private static string _portal;
		public static int Counter { get; set; }

		public static string Portal { get; set; }

		public static string Login { get; set; }
		public static string Hash { get; set; }
		public static string Key { get; set; }

		
		/// <summary> Токен </summary>
		public static string Token { get; set; }

		/// <summary> Мастер токен </summary>
		public static string MasterToken { get; set; }

		/// <summary> Логин к Yandex </summary>
		public static string YandexLogin { get; set; }


		public static string PreparePortal(string portal)
		{
			portal =
				portal.Replace("https", string.Empty)
					.Replace("http", string.Empty)
					.Replace(" ", string.Empty)
					.Replace("/", string.Empty);

			return "https://" + portal;
		}


		//_____________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Установить параметры подключения </summary>
		////=====================================================
		public static void SetCredentials(string host)
		{
			Loger.AddLog("AuthData.SetCredentials");

			//Loger.AddLog("host=" + host, true);
			//Loger.AddLog("login=" + login, true);
			//Loger.AddLog("hash=" + hash, true);
			
			
			var chunks = host.Replace("||", "|").Split('|');
			if (chunks.Count() >= 2)
			{
				//Portal = chunks[0];
				//Key = chunks[1];
				Token = chunks[0];
				MasterToken = chunks[1];
				YandexLogin = chunks[2];

				Loger.AddLog("Token=" + Token, true);
				Loger.AddLog("MasterToken=" + MasterToken, true);
				Loger.AddLog("YandexLogin=" + YandexLogin, true);
			}

			//Login = login;
			//Hash = hash;

			Loger.AddLog("AuthData.SetCredentials.End");
		}

		public static bool CheckCredentials()
		{
			return !(string.IsNullOrWhiteSpace(Portal) || string.IsNullOrWhiteSpace(Login) || string.IsNullOrWhiteSpace(Hash));
		}

		
	}
}
