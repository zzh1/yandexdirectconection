﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using QlikView.Qvx.QvxLibrary;

namespace QvYandexDirectConnector
{
	internal class QvYandexDirectConnection : QvxConnection
	{
		//______________________________________________________________________________________________________________________________________________________________________________________
		public override void Init()
		{
			Loger.AddLog("QvYandexDirectConnection.Init");
			
			AuthData.Counter++;

			// Получить параметры подключения
			var hasParams = MParameters != null && MParameters.Any();
			
			// Если параметры "приехали", а не ложный вызов
			if (hasParams)
			{
				Loger.AddLog("hasParams", true);
				
				// Сохранить входные данные
				AuthData.SetCredentials(MParameters["host"]);
				
			}
			else
			{
				Loger.AddLog("not Params", true);
			}

			MTables = GetTable();
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить тиблицы в формате КликСайнса </summary>
		////==========================================================
		private List<QvxTable> GetTable()
		{
			Loger.AddLog("QvYandexDirectConnection.GetTable()");
			
			var ydt = new YandexDirectTable();
			List<QvxTable> result = ydt.GetTable();

			Loger.AddLog("result.count=" + result.Count, true);

			return result;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить события приложения в формате КликСайнса </summary>
		////=====================================================================
		private IEnumerable<QvxDataRow> GetApplicationEvents()
		{
			var ev = new EventLog("Application");

			foreach (var evl in ev.Entries)
			{
				yield return MakeEntry(evl as EventLogEntry, FindTable("ApplicationsEventLog",
				MTables));
			}
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		private QvxDataRow MakeEntry(EventLogEntry evl, QvxTable table)
		{
			var row = new QvxDataRow();
			row[table.Fields[0]] = evl.Category;
			row[table.Fields[1]] = evl.EntryType.ToString();
			row[table.Fields[2]] = evl.Message;
			return row;
		}
	}
}
