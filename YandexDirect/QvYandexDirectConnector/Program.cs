﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace QvYandexDirectConnector
{
	static class Program
	{
		/// <summary> The main entry point for the application. </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if (args != null && args.Length >= 2)
			{
				// Используем data piped to Qlik Sense
				new QvYandexDirectServer().Run(args[0], args[1]);
			}

			
			//Application.EnableVisualStyles();
			//Application.SetCompatibleTextRenderingDefault(false);
			//Application.Run(new Form1());
		}
	}
}
