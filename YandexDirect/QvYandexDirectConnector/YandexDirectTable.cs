﻿using System.Collections.Generic;
using QlikView.Qvx.QvxLibrary;
using YandexDirectUsing;

namespace QvYandexDirectConnector
{
	class YandexDirectTable
	{
		private QvxTable table;

		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Передать таблицу в формате QS </summary>
		////==================================================
		public List<QvxTable> GetTable()
		{
			Loger.AddLog("YandexDirectTable.GetTable()");
			
			var result = new List<QvxTable>();
			table = new QvxTable
			{
				TableName = "YandexDirectSummaryStatInfo", 
				Fields = GetFields(), 
				GetRows = GetRows
			};
			
			result.Add(table);

			return result;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		protected IEnumerable<QvxDataRow> GetRows()
		{
			Loger.AddLog("YandexDirectTable.GetRows()");
			
			var rows = new List<QvxDataRow>();

			// Инициализировать объект доступа к Yandex Директу через репазиторий
			var repositoryProcessing = new RepositoryProcessing(AuthData.YandexLogin, AuthData.Token, AuthData.MasterToken);
			// Получить данные (возможные даты высчитать автоматически)
			List<GetSummaryStatInfo> listInfo = repositoryProcessing.GetSummaryStatFrom();

			if (listInfo != null)
			{
				Loger.AddLog("row = " + listInfo.Count.ToString(), true);
				
				// Заполнить поля
				foreach (var getSummaryStatInfo in listInfo)
				{
					var row = new QvxDataRow();

					row[table.Fields[0]] = getSummaryStatInfo.CampaignId.ToString();
					row[table.Fields[1]] = getSummaryStatInfo.ClicksContext.ToString();
					row[table.Fields[2]] = getSummaryStatInfo.ClicksSearch.ToString();
					row[table.Fields[3]] = getSummaryStatInfo.GoalConversionContext.ToString();
					row[table.Fields[4]] = getSummaryStatInfo.GoalConversionSearch.ToString();
					row[table.Fields[5]] = getSummaryStatInfo.GoalCostContext.ToString();
					row[table.Fields[6]] = getSummaryStatInfo.GoalCostSearch.ToString();
					row[table.Fields[7]] = getSummaryStatInfo.SessionDepthContext.ToString();
					row[table.Fields[8]] = getSummaryStatInfo.SessionDepthSearch.ToString();
					row[table.Fields[9]] = getSummaryStatInfo.ShowsContext.ToString();
					row[table.Fields[10]] = getSummaryStatInfo.ShowsSearch.ToString();
					row[table.Fields[11]] = getSummaryStatInfo.SumContext.ToString();
					row[table.Fields[12]] = getSummaryStatInfo.SumSearch.ToString();
					row[table.Fields[13]] = getSummaryStatInfo.StatDate.ToShortDateString() + " " + getSummaryStatInfo.StatDate.ToShortTimeString();

					rows.Add(row);
				}
			}
			else
			{
				Loger.AddLog("Yandex ret null", true);
			}

			return rows;
		}

		//______________________________________________________________________________________________________________________________________________________________________________________
		protected QvxField[] GetFields()
        {
			Loger.AddLog("YandexDirectTable.GetFields()");

            var fields = new List<QvxField>
            {
                
				//CreateQvxField("CompanyId"),
				//CreateQvxField("ClickCountInYandexAdv"),
				//CreateQvxField("ClickCountInSerch"),
				//CreateQvxField("PartVisitFromYandexAdv"),
				//CreateQvxField("PartVisitFromYandexSerch"),
				//CreateQvxField("PriceToGoalFromYandexAdv"),
				//CreateQvxField("PriceToGoalFromYandexSerch"),
				//CreateQvxField("DepthSiteLookFromYandexAdv"),
				//CreateQvxField("DepthSiteLookFromYandexSerch"),
				//CreateQvxField("CountShowInYandexAdv"),
				//CreateQvxField("CountShowInYandexSerch"),
				//CreateQvxField("CostClicsInYandexAdv"),
				//CreateQvxField("CostClicsInYandexSerch"),
				//CreateQvxField("StatDate"),
				
				
				CreateQvxField("Идентификатор_компании"),
				CreateQvxField("Количество_кликов_в_Рекламной_сети_Яндекса"),
				CreateQvxField("Количество_кликов_на_поиске"),
				CreateQvxField("Доля_цел_виз_из_Рекл_сети_Ян_в_проц"),
				CreateQvxField("Доля_цел_виз_при_переходе_с_поиска_в_проц"),
				CreateQvxField("Цена_достиж_цели_Ян_Метр_Рекл_сети_Яна"),
				CreateQvxField("Цена_достиж_цели_Ян_Метр_при_перех_с_поиса"),
				CreateQvxField("Глубина_просм_сайта_из_Рекл_сети_Ян"),
				CreateQvxField("Глубина_просм_сайта_при_переходе_с_поиска"),
				CreateQvxField("Количество_показов_в_Рекламной_сети_Янд"),
				CreateQvxField("Количество_показов_на_поиске"),
				CreateQvxField("Стоимость_кликов_в_Рекламной_сети_Янд"),
				CreateQvxField("Стоимость_кликов_на_поиске"),
				CreateQvxField("Дата_за_которую_приведена_статистика"),
            };

            return fields.ToArray();
        }
		


		//______________________________________________________________________________________________________________________________________________________________________________________
		protected QvxField CreateQvxField(string name, QvxFieldType type = QvxFieldType.QVX_TEXT, FieldAttrType attrType = FieldAttrType.ASCII, QvxNullRepresentation representation = QvxNullRepresentation.QVX_NULL_FLAG_SUPPRESS_DATA)
        {
            return new QvxField(name, type, representation, attrType);
        }

	}
}
