﻿define( ['qvangular',
		'text!/customdata/32/YandexDirectConnector/web/log_page.html',
		'css!/customdata/32/YandexDirectConnector/web/log_page.css'
], function ( qvangular, template) {
	return {
		template: template,
		controller: ['$scope', 'input', function ( $scope, input ) {
		    function init()
		    {
				$scope.isEdit = input.editMode;
				$scope.id = input.instanceId;
				$scope.titleText = $scope.isEdit ? "Изменить подключение к AmoCRM" : "Подключиться к AmoCRM";
				$scope.saveButtonText = $scope.isEdit ? "Сохранить изменения" : "Создать";

				$scope.name = "Яндекс Директ";
				//$scope.username = "try";
				//$scope.password = "me";
				//$scope.portal_url = "https://test-rasshireniya-7.amocrm.ru";
			    //$scope.license_key = "456668778-44515";
				$scope.provider = "QvYandexDirectConnector.exe"; // Connector filename
				$scope.connectionInfo = "";
				$scope.connectionSuccessful = false;
				$scope.connectionString = createCustomConnectionString($scope.provider, "host=" + $scope.portal_url);

				/*input.serverside.sendJsonRequest( "getInfo" ).then( function ( info ) {
					$scope.info = info.qMessage;
				} );*/

		        if ($scope.isEdit)
				{
					input.serverside.getConnection( $scope.id ).then( function ( result )   {
						                                                                        $scope.name = result.qName;
					                                                                        } );
				}
		    }
		    
		    $scope.rebuildCS = function ()//yandexLogin
		    {
		        $scope.connectionString = createCustomConnectionString($scope.provider, "host=" + $scope.token + "||masterToken||" + $scope.yandexLogin);
		    };


		    $scope.wtfFunc = function() {
		                                    return 1;
		                                };

		    /* Event handlers */

			$scope.onOKClicked = function ()
			{
			    if ($scope.isEdit)
			    {
			        var overrideCredentials = true;
			        
					input.serverside.modifyConnection(  $scope.id,
						                                $scope.name,
						                                $scope.connectionString,
						                                $scope.provider,
						                                overrideCredentials,
						                                '',
						                                '' ).then( function ( result ) {
						                                                                                if (result)
						                                                                                {
								                                                                            $scope.destroyComponent();
							                                                                            }
						                                                                            }
						                                );
			    }
			    else
			    {
					input.serverside.createNewConnection( $scope.name, $scope.connectionString, '', '');
					$scope.destroyComponent();
				}
			};

            //_________________________________________________________________________________________________________________________________________________________________________
		    // При клике "Получить токен"
			$scope.onGetToken = function ()
			{
			    var promise = input.serverside.sendJsonRequest("getToken");

			    promise.catch(function (a) {
			        console.log("Catched ex");
			        console.log(a);
			        $scope.connectionInfo = "Не удалось получить token=";
			    });

			    promise.then(function (info)
			                 {
			                    $scope.token = info.qMessage;
			                });
			};


		    //_________________________________________________________________________________________________________________________________________________________________________
		    // При клике "TestConnection"
			$scope.onTestConnectionClicked = function ()
			{
			    console.log("testConnection init");

			    var promise = input.serverside.sendJsonRequest("testConnection", $scope.yandexLogin, $scope.token);

			    console.log(promise);
			    console.log("testConnection promise generated");

			    promise.catch(function (a)
			    {
			        console.log("Catched ex");
			        console.log(a);
			    });

			    promise.then(function (info)
			    {
					$scope.connectionInfo = info.qMessage;
					$scope.connectionSuccessful = info.qMessage == "Все OK!";
				} );
			};

			$scope.isOkEnabled = function ()
			{
			    return !($scope.yandexLogin.length > 0 && $scope.connectionSuccessful);
			};

			$scope.onEscape = $scope.onCancelClicked = function ()
			{
				$scope.destroyComponent();
			};

			
			/* Helper functions */

			function createCustomConnectionString(filename, connectionstring)
			{
				return "CUSTOM CONNECT TO " + "\"provider=" + filename + ";" + connectionstring + "\"";
			}


			init();
		}]
	};
} );