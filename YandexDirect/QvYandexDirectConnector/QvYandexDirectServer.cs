﻿
using System.Windows.Forms;
using QlikView.Qvx.QvxLibrary;

namespace QvYandexDirectConnector
{
	internal class QvYandexDirectServer : QvxServer
	{
		public override string CreateConnectionString()
		{
			Loger.AddLog("QvYandexDirectServer.CreateConnectionString");
			
			QvxLog.Log(QvxLogFacility.Application, QvxLogSeverity.Debug, "CreateConnectionString()");
			return "Server=localhost";
		}

		public override QvxConnection CreateConnection()
		{
			Loger.AddLog("QvYandexDirectServer.CreateConnection");
			
			return new QvYandexDirectConnection();
		}

		public override string HandleJsonRequest(string method, string[] userParameters, QvxConnection connection)
		{
			Loger.AddLog("QvYandexDirectServer.HandleJsonRequest");
			//Loger.AddLog(JsonConvert.SerializeObject(userParameters), true);
			
			//LogFileTempFolder.Log("userParametres", JsonConvert.SerializeObject(userParameters));
			QvDataContractResponse response;

			QvxLog.Log(QvxLogFacility.Application, QvxLogSeverity.Debug, "Handle json Request > " + method);

			string provider, host, username, password, token, masterToken;

			connection.MParameters.TryGetValue("provider", out provider);			// Set to the name of the connector by QlikView Engine
			connection.MParameters.TryGetValue("userid", out username);				// Set when creating new connection or from inside the QlikView Management Console (QMC)
			connection.MParameters.TryGetValue("password", out password);			// Same as for email
			connection.MParameters.TryGetValue("host", out host);					// Defined when calling createNewConnection in connectdialog.js

			switch (method)
			{
				case "getInfo":
					response = getInfo();
					break;
				case "getDatabases":
					response = getDatabases(username, password);
					break;
				case "getTables":
					response = getTables(username, password, connection, userParameters[0], userParameters[1]);
					break;
				case "getFields":
					response = getFields(username, password, connection, userParameters[0], userParameters[1], userParameters[2]);
					break;
				case "testConnection":
					string yandxLogin = userParameters[0];
					string yandxToken = userParameters[1];
					response = TestConnection(yandxLogin, yandxToken);
					break;
				case "getToken":
					response = GetToken();
					break;
				default:
					response = new Info { qMessage = "Неизвестная команда" };
					break;
			}
			return ToJson(response);    // serializes response into JSON string

		}

		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Вернуть Yandex Token по логину </summary>
		////===================================================
		private QvDataContractResponse GetToken()
		{
			// Открвть форму вторизации
			var form = new WebAuth
						{
							TopMost = true
						};
			form.ShowDialog();

			Loger.AddLog("code=" + form.AccessToken);

			// Получить из формы токен
			var token = new Info
			{
				qMessage = form.AccessToken
			};
			
			return token;
		}


		public QvDataContractResponse getInfo()
		{
			return new Info
			{
				qMessage = "Используйте логин и API-ключ для соеднинения с порталом Amo"
			};
		}


		public QvDataContractResponse getDatabases(string username, string password)
		{
			//if (verifyCredentials(username, password))
			//{
				return new QvDataContractDatabaseListResponse
				{
					qDatabases = new[]
                    {
                        new Database {qName = "AmoCRM"}
                    }
				};
			//}
			return new Info { qMessage = "Не удалось подключиться!" };
		}

		public QvDataContractResponse getTables(string username, string password, QvxConnection connection, string database, string owner)
		{
			//if (verifyCredentials(username, password))
			//{
				connection.Init();
				//LogFileTempFolder.Log("connection_info", JsonConvert.SerializeObject(connection));
				return new QvDataContractTableListResponse
				{
					qTables = connection.MTables
				};
			//}
			return new Info { qMessage = "Credentials WRONG!" };
		}

		public QvDataContractResponse getFields(string username, string password, QvxConnection connection, string database, string owner, string table)
		{
			//if (verifyCredentials(username, password))
			//{
				connection.Init();
				var currentTable = connection.FindTable(table, connection.MTables);

				return new QvDataContractFieldListResponse
				{
					qFields = (currentTable != null) ? currentTable.Fields : new QvxField[0]
				};
			//}
			return new Info { qMessage = "Credentials WRONG!" };
		}


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Проверка соединения </summary>
		////========================================
		private QvDataContractResponse TestConnection(string yandexLogin, string yandexToken)
		{
			Info retMessage = null;
			
			if (string.IsNullOrWhiteSpace(yandexLogin))
			{
				retMessage = new Info { qMessage = "Не заполнено поле 'Логин Yandex`а'" };
			}
			else if (string.IsNullOrWhiteSpace(yandexToken))
			{
				retMessage = new Info {qMessage = "Не заполнено поле 'Токен'"};
			}
			else
			{
				var message = "Не получилось подключиться к AmoCRM. Проверьте данные еще раз";

				if (TryConnectToAmo(yandexLogin, yandexToken))
				{
					message = "Все OK!";

					//if (!KeyGenerator.CheckKey(portal, key)) message += " Но вы можете загрузить не более 20 сделок, так как лицензионный ключ недействителен";
				}

				retMessage = new Info { qMessage = message };
			}

			return retMessage;
		}


		//________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Проверка подключения (оставлена для возможных доработок) </summary>
		////=============================================================================
		private bool TryConnectToAmo(string yandexLogin, string yandexToken)
		{
			Loger.AddLog("TryConnectToAmo");
			
			//var connectResult = new Amo(portal, email, hash).Connect();
			return true;
		}

	}


}
