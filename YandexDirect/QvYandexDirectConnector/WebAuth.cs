﻿using System;
using System.Windows.Forms;
using Newtonsoft.Json;
using RestSharp;

namespace QvYandexDirectConnector
{
    public partial class WebAuth : Form
    {
        public readonly string ClientId = "6b0da456d0d04bd285da35d96015573b";
        public readonly string ClientSecret = "0b9d70fe0ba04f669efc1ebd61b7812e";

        public string AccessToken { get; set; }

        public WebAuth()
        {
            InitializeComponent();
        }


		//_______________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Открыть браузер и перейти на страницу </summary>
		////==========================================================
        private void WebAuthLoad(object sender, EventArgs e)
        {
            // Открываем браузер на странице авторизации, чтобы
            // пользователь мог ввести логин / пароль и дать доступ
            // нашему приложению.

            var url = string.Format("https://oauth.yandex.ru/authorize?response_type=code&client_id={0}", ClientId);

            // При открытии новой страницы проверяем, что адрес страницы
            // начинается с 'redirect_uri'. 
            webBrowser.Navigated += (o, args) => CheckVerificationCode(webBrowser.Url.AbsoluteUri);
            webBrowser.Navigate(url);
        }


		//_______________________________________________________________________________________________________________________________________________________________________________
		/// <summary>  Получает проверочный code, если мы перешли на 'redirect_uri' </summary>
        /// <param name="url">Текущий адрес браузера</param>
		///===================================================================================
        private void CheckVerificationCode(string url)
        {
            // Это 'redirect_uri' из настроек нашего приложения 
            // + сразу прописан параметр 'code' для уобства работы.
            string targetPage = "https://oauth.yandex.ru/verification_code?code=";

            if (!url.StartsWith(targetPage)) return;

            string code = url.Replace(targetPage, string.Empty);
            AccessToken = GetAccessToken(code);

            Close();
        }


		//_______________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получает 'access_token' </summary>
        /// <param name="code">Проверочный код, полученный на первом шаге авторизации</param>
        /// <returns>Токен для работы с API</returns>
		///==================================================================================
        private string GetAccessToken(string code)
        {
            var client = new RestClient("https://oauth.yandex.ru/token");

            var request = new RestRequest(Method.POST);
            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("code", code);
            request.AddParameter("client_id", ClientId);
            request.AddParameter("client_secret", ClientSecret);

            IRestResponse response = client.Execute(request);

            var deserializedResponse = JsonConvert.DeserializeAnonymousType(
                response.Content,
                new {access_token = string.Empty});

            return deserializedResponse.access_token;
        }
    }
}
