﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using QlikView.Qvx.QvxLibrary;

namespace QvYandexDirectConnector
{
	internal class QvYandexDirectConnection : QvxConnection
	{
		public override void Init()
		{

			AuthData.Counter++;

			// Получить параметры подключения
			var hasParams = MParameters != null && MParameters.Any();

			// Если параметры "приехали", а не ложный вызов
			if (hasParams)
			{
				//LogFileTempFolder.Log("parametres_info", JsonConvert.SerializeObject(MParameters));
				
				// Сохранить входные данные
				AuthData.SetCredentials(MParameters["host"], MParameters["UserId"], MParameters["Password"]);

				AuthData.Token = MParameters["token"];
				AuthData.Token = MParameters["masterToken"];
			}

			MTables = GetTable();
		}


		private List<QvxTable> GetTable()
		{
			var ydt = new YandexDirectTable();
			List<QvxTable> result = ydt.GetTable();

			return result;
		}



		private IEnumerable<QvxDataRow> GetApplicationEvents()
		{
			var ev = new EventLog("Application");

			foreach (var evl in ev.Entries)
			{
				yield return MakeEntry(evl as EventLogEntry, FindTable("ApplicationsEventLog",
				MTables));
			}
		}

		private QvxDataRow MakeEntry(EventLogEntry evl, QvxTable table)
		{
			var row = new QvxDataRow();
			row[table.Fields[0]] = evl.Category;
			row[table.Fields[1]] = evl.EntryType.ToString();
			row[table.Fields[2]] = evl.Message;
			return row;
		}
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                             ring json = jss.Serialize(ClientInfo);

			// адрес для отправки json-запросов
			//string jurl = "https://api.direct.yandex.ru/v4/json/";
			string jurl = "https://api-sandbox.direct.yandex.ru/v4/json/";


			// SSL-сертификат не используется
			ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);

			// создаем клиента
			WebClient wc = new WebClient();

			// отправляем POST-запрос и получаем ответ
			byte[] result = wc.UploadData(jurl, "POST", System.Text.Encoding.UTF8.GetBytes(json));


			outpuTextBox.Text = " Запрос: \n" + json;
			inpuTextBox.Text = "\n Ответ: \n" + Encoding.UTF8.GetString(result);
			
		}

		private void ButtonClick(object sender, RoutedEventArgs e)
		{
			/***** Входные данные  *****/

			// логин пользователя
			string usr_login = "zzh1";
			
			string api_method = "GetCreditLimits";
			
			
			// мастер-токен
			//string master_token = "xxxxiGgKNQ3k4qnH";
			// счетчик операций
			string op_num = "4";

			/***************************/

			// адрес для отправки json-запросов
			string jurl = "https://api-sandbox.direct.yandex.ru/v4/json/";

			// генерация финансового токена
			SHA256Managed sha = new SHA256Managed();
			string f_data = MasterToken + op_num + api_method + usr_login;
			byte[] f_hash = sha.ComputeHash(Encoding.UTF8.GetBytes(f_data));
			string f_token = "";
			foreach (byte b in f_hash)
			{
				f_token += String.Format("{0:x2}", b);
			}

			// входная структура
			var CreditLimitsInfo = new
			{
				// авторизационный токен
				token = Token,
				// метод API
				method = api_method,
				// финансовый токен
				finance_token = f_token,
				// счетчик операций
				operation_num = op_num,
			};

			// сериализуем объект CreditLimitsInfo в формат нотации JSON
			JavaScriptSerializer jss = new JavaScriptSerializer();
			string json = jss.Serialize(CreditLimitsInfo);

			// SSL-сертификат не используется
			ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);

			// создаем клиента
			WebClient wc = new WebClient();

			// отправляем POST-запрос и получаем ответ
			byte[] result = wc.UploadData(jurl, "POST", System.Text.Encoding.UTF8.GetBytes(json));

			outpuTextBox.Text = " Запрос: \n" + json;
			inpuTextBox.Text = "\n Ответ: \n" + Encoding.UTF8.GetString(result);

		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var yandexDirect = new YandexDirectInfo("zzh1", Token, MasterToken);
			yandexDirect.GetSummaryStat(DateTime.Now.AddDays(-5), DateTime.Now);
		}

		
	}
}
