﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows;
using Newtonsoft.Json;
using YandexDirectUsing;


namespace TestApp
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void TestButtonClick(object sender, RoutedEventArgs e)
		{
			string yandexLogin = loginTextBox.Text;
			string yandexToken = tokenTextBox.Text;
			string yandexMasterToken = "12345";
			
			// Инициализировать объект доступа к Yandex Директу
			var yandexDirect = new YandexDirectUsing.YandexDirectInfo(yandexLogin, yandexToken, yandexMasterToken);
			// Получить данные
			List<YandexDirectUsing.GetSummaryStatInfo> listInfo = yandexDirect.GetSummaryStat();

			int conut = listInfo.Count;
			string answer = yandexDirect.DebugServerAnswer;
		}

		private void TestJsonButtonClick(object sender, RoutedEventArgs e)
		{
			List<TestJson> items = GetTestJsons();

			Stopwatch stopWatch2 = new Stopwatch();
			stopWatch2.Start();

			var sz = JsonConvert.SerializeObject(items);
			var f = JsonConvert.DeserializeObject(sz);
			
			stopWatch2.Stop();

			// Get the elapsed time as a TimeSpan value.
			TimeSpan ts2 = stopWatch2.Elapsed;



			Stopwatch stopWatch1 = new Stopwatch();
			stopWatch1.Start();
			
			var jss = new JavaScriptSerializer();
			var json = jss.Serialize(items);
			var g = jss.Deserialize<List<TestJson>>(json);
			
			stopWatch1.Stop();
			// Get the elapsed time as a TimeSpan value.
			TimeSpan ts = stopWatch1.Elapsed;




			// Format and display the TimeSpan value.
			string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);


			



			// Format and display the TimeSpan value.
			string elapsedTime2 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts2.Hours, ts2.Minutes, ts2.Seconds, ts2.Milliseconds / 10);


			MessageBox.Show("t1=" + elapsedTime + "\r\n" + "t2=" + elapsedTime2 + "\r\n");

		}

		private class TestJson
		{
			public string Name;
			public string Type;
			public string Title;
			public int Count;
			public int Weight;
		}


		private List<TestJson> GetTestJsons()
		{
			var items = new List<TestJson>();
			
			for (int i = 0; i < 10000; i++)
			{
				items.Add(new TestJson
				{
					Count = 10,
					Name = "Всё началось в далёком 2008 году ",
					Title = "результаты превзошли все ожидания",
					Type = "содержание основывается на понимании работы",
					Weight = 33
				});
			}

			return items;
		}

		private void TestFromRepButtonClick(object sender, RoutedEventArgs e)
		{

			string yandexLogin = loginTextBox.Text;
			string yandexToken = tokenTextBox.Text;
			string yandexMasterToken = "12345";
			
			// Инициализировать объект доступа к Yandex Директу через репазиторий
			var repositoryProcessing = new RepositoryProcessing(yandexLogin, yandexToken, yandexMasterToken);
			// Получить данные (возможные даты высчитать автоматически)
			List<GetSummaryStatInfo> listInfo = repositoryProcessing.GetSummaryStatFrom();


		}

	}
}
