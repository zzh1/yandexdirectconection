﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using YandexDirectUsing.Repository;

namespace YandexDirectUsing
{
	
	public struct RepositorySummaryStat
	{
		/// <summary> Статистика </summary>
		public List<GetSummaryStatInfo> SummaryStat;
		/// <summary> Конечная граница даты получения </summary>
		public DateTime EndTimeBorder;
		/// <summary> Начальная граница даты получения </summary>
		public DateTime StartTimeBorder;
	}


	public class RepositoryProcessing
	{
		private const string FILE_NAME = "items.xml";
		
		/// <summary> Конфигурация репазитория </summary>
		private RepositoryConfig repositoryConfig;

		/// <summary> Имя пользователя </summary>
		public string UserName { get; set; }

		/// <summary> Токен </summary>
		public string Token { get; set; }

		/// <summary> Мастер токен </summary>
		public string MasterToken { get; set; }


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Конструктор класса </summary>
		////=======================================
		public RepositoryProcessing(string userName, string token, string masterToken)
		{
			// Проинициализиторать экземпляр конфигурации
			repositoryConfig = new RepositoryConfig();

			UserName = userName;
			Token = token;
			MasterToken = masterToken;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Инфа по компаниям с авто расчётом времени </summary>
		////==============================================================
		public List<GetSummaryStatInfo> GetSummaryStatFrom()
		{
			var yandexDirectInfo = new YandexDirectInfo(UserName, Token, MasterToken);
			
			// Нижняя граница получения данных
			DateTime downBorderDate = DateTime.Now;
			// Верхняя граница получения данных
			DateTime upBorderDate = DateTime.Now;

			// Получить данные из репазитория
			RepositorySummaryStat repositorySummaryStat = LoadFromRep();

			if (repositorySummaryStat.SummaryStat.Count == 0)
			{
				ContainRepository();
			}
			else
			{
				upBorderDate = repositorySummaryStat.EndTimeBorder;


				// Пока текщая нижняя граница запроса не будет ниже даты из репозитория (то, на чём заканчиваются данные сверху
				// или не будет привышен лимит запроса на сутки
				while (downBorderDate > upBorderDate || (repositoryConfig.LastGetInfoTime == DateTime.Now && repositoryConfig.GetInfoCount < 90))
				{
					var currDate = DateTime.Now;

					// Если прошлый запрос был не сегодня
					if (repositoryConfig.LastGetInfoTime.Year != currDate.Year && repositoryConfig.LastGetInfoTime.Month != currDate.Month
					    && repositoryConfig.LastGetInfoTime.Day != currDate.Day)
					{
						// Установить, последний запрос в "сегодня"
						repositoryConfig.LastGetInfoTime = currDate;
						// Установить кол-во запросов "сегодня" в 0
						repositoryConfig.GetInfoCount = 0;
					}

					// Получить текущие данные из Yandex`а
					RepositorySummaryStat curentSummaryStat = yandexDirectInfo.GetSummaryStatFrom(downBorderDate);

					// Увеличить кол-во запросов за сегодня
					repositoryConfig.GetInfoCount++;

					// Сохранить нижнее время
					downBorderDate = curentSummaryStat.StartTimeBorder;

					// Если кол-во статистик >0, то сохранить, иначе выйти, т.к. ниже нет
					if (curentSummaryStat.SummaryStat.Count > 0)
					{
						// Запомнить верхнюю границу
						curentSummaryStat.EndTimeBorder = DateTime.Now;

						// Добавить в репазиторий данные
						AddToRep(curentSummaryStat);
					}
					else
					{
						break;
					}
				}

				// Сохранить данные конфигурации
				repositoryConfig.Save();
			}

			// Получить обновлённые данные из репазитория
			repositorySummaryStat = LoadFromRep();
			
			// Вернуть результирующий набор
			return repositorySummaryStat.SummaryStat;
		}


		private void ContainRepository()
		{
			var yandexDirectInfo = new YandexDirectInfo(UserName, Token, MasterToken);

			// Верхняя граница получения данных
			DateTime downBorderDate = DateTime.Now;
			
			// Пока текщая нижняя граница запроса не будет ниже даты из репозитория или не будет привышен лимит запроса на сутки
			while (repositoryConfig.LastGetInfoTime == DateTime.Now && repositoryConfig.GetInfoCount < 90)
			{
				var currDate = DateTime.Now;

				// Если прошлый запрос был не сегодня
				if (repositoryConfig.LastGetInfoTime.Year != currDate.Year && repositoryConfig.LastGetInfoTime.Month != currDate.Month 
					&& repositoryConfig.LastGetInfoTime.Day != currDate.Day)
				{
					// Установить, последний запрос в "сегодня"
					repositoryConfig.LastGetInfoTime = currDate;
					// Установить кол-во запросов "сегодня" в 0
					repositoryConfig.GetInfoCount = 0;
				}

				// Получить текущие данные из Yandex`а
				RepositorySummaryStat curentSummaryStat = yandexDirectInfo.GetSummaryStatFrom(downBorderDate);

				// Увеличить кол-во запросов за сегодня
				repositoryConfig.GetInfoCount++;

				// Сохранить нижнее время, на день раньше, для следующего запроса
				downBorderDate = curentSummaryStat.StartTimeBorder.AddDays(-1);

				// Если кол-во статистик >0, то сохранить, иначе выйти, т.к. ниже нет
				if (curentSummaryStat.SummaryStat.Count > 0)
				{
					curentSummaryStat.EndTimeBorder = DateTime.Now;
					
					// Добавить в репазиторий данные
					AddToRep(curentSummaryStat);
				}
				else
				{
					break;
				}
			}

			// Сохранить данные конфигурации
			repositoryConfig.Save();
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Добавить в репазиторий </summary>
		////===========================================
		private void AddToRep(RepositorySummaryStat repositorySummaryStat)
		{
			// Получить текущие данные, без данных с начальной датой
			RepositorySummaryStat curentSummaryStat = LoadFromRepAndRemoveLastDay(repositorySummaryStat.StartTimeBorder);
			
			// Добавить в полученный набор, список на добавление
			foreach (GetSummaryStatInfo summaryStat in repositorySummaryStat.SummaryStat)
			{
				curentSummaryStat.SummaryStat.Add(summaryStat);
			}

			// Сериализуем объект CreditLimitsInfo в формат нотации JSON
			string sz = JsonConvert.SerializeObject(curentSummaryStat);

			var fi = new FileInfo(FILE_NAME);

			StreamWriter sw = fi.AppendText();

			sw.Write(sz);
			sw.Close();
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить данные из репазитория и удалить заданный день </summary>
		////===========================================================================
		private RepositorySummaryStat LoadFromRepAndRemoveLastDay(DateTime lastDay)
		{
			// Получить текущие данные
			RepositorySummaryStat curentSummaryStat = LoadFromRep();
			
			// Новый набор данных
			var newSummaryStat = new List<GetSummaryStatInfo>();
			
			// Выбрать все без заданной даты
			foreach (GetSummaryStatInfo summaryStat in curentSummaryStat.SummaryStat)
			{
				if (summaryStat.StatDate != lastDay)
				{
					newSummaryStat.Add(summaryStat);
				}
			}

			// Вернуть итог
			return new RepositorySummaryStat
			{
				EndTimeBorder = curentSummaryStat.EndTimeBorder,
				SummaryStat = newSummaryStat
			};
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Загрузить из репазитория (файла) </summary>
		////=====================================================
		private RepositorySummaryStat LoadFromRep()
		{
			RepositorySummaryStat ret;

			var fi = new FileInfo(FILE_NAME);
			if (fi.Exists)
			{
				// Прочитать xml из файла
				StreamReader sr = fi.OpenText();
				string sz = sr.ReadToEnd();
				sr.Close();
				try
				{
					// Десериализовать
					ret = JsonConvert.DeserializeObject<RepositorySummaryStat>(sz);
				}
				catch 
				{
					ret = new RepositorySummaryStat
					{
						SummaryStat = new List<GetSummaryStatInfo>()
					};
				}
			}
			else
			{
				ret = new RepositorySummaryStat
				{
					SummaryStat = new List<GetSummaryStatInfo>()
				};
			}

			return ret;
		}
	}
}
