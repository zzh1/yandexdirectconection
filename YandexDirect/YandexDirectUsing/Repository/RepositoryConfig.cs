﻿using System;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

namespace YandexDirectUsing.Repository
{
	/// <summary> Конфигурация репазитория </summary>
	public class RepositoryConfig
	{
		private const string FILE_NAME = "RepositoryConfig.xml";
		
		/// <summary> Последняя дата получения данных </summary>
		public DateTime LastGetInfoTime { get; set; }

		/// <summary> Кол-во запросов в эту дату </summary>
		public int GetInfoCount  { get; set; }


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Конструктор класса </summary>
		////=======================================
		public RepositoryConfig(bool isLoadParams = true)
		{
			if (isLoadParams)
			{
				// Загрузить данные 
				Load();
			}
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Загрузить данные конфига из файла </summary>
		////====================================================
		public void Load()
		{
			// Получить экземпляр без загрузки данных 
			var ret = new RepositoryConfig(false);

			var fi = new FileInfo(FILE_NAME);
			if (fi.Exists)
			{
				// Прочитать xml из файла
				StreamReader sr = fi.OpenText();
				string sz = sr.ReadToEnd();
				sr.Close();
				// Десериализовать
				try
				{
					ret = JsonConvert.DeserializeObject<RepositoryConfig>(sz);
				}
				catch 
				{
				}
				
			}

			LastGetInfoTime = ret.LastGetInfoTime;
			GetInfoCount = ret.GetInfoCount;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Сохранить данные конфига в файл </summary>
		////====================================================
		public void Save()
		{

			// Сериализуем объект CreditLimitsInfo в формат нотации JSON
			string sz = JsonConvert.SerializeObject(this);

			var fi = new FileInfo(FILE_NAME);

			//if (!fi.Exists)
			//{
			//	fi.Create();
			//	// Подождать, пока отработает файловая система
			//	Thread.Sleep(5000);
			//}

			StreamWriter sw = fi.AppendText();

			sw.Write(sz);
			sw.Close();
		
		}
	}
}
