﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YandexDirectUsing
{
	public struct GetSummaryStatInfo
	{
		/// <summary> Идентификатор коомпании </summary>
		public int CampaignId;
		/// <summary> Дата, за которую приведена статистика </summary>
		public DateTime StatDate;
		/// <summary> Стоимость кликов на поиске </summary>
		public float SumSearch;
		/// <summary> Стоимость кликов в Рекламной сети Яндекса </summary>
		public float SumContext;
		/// <summary> Количество показов на поиске </summary>
		public int ShowsSearch;
		/// <summary> Количество показов в Рекламной сети Яндекса </summary>
		public int ShowsContext;
		/// <summary> Количество кликов на поиске </summary>
		public int ClicksSearch;
		/// <summary> Количество кликов в Рекламной сети Яндекса </summary>
		public int ClicksContext;
		/// <summary> Глубина просмотра сайта при переходе с поиска </summary>
		public float SessionDepthSearch;
		/// <summary> Глубина просмотра сайта при переходе из Рекламной сети Яндекса </summary>
		public float SessionDepthContext;
		/// <summary> Доля целевых визитов в общем числе визитов при переходе с поиска, в процентах </summary>
		public float GoalConversionSearch;
		/// <summary> Доля целевых визитов в общем числе визитов при переходе из Рекламной сети Яндекса, в процентах </summary>
		public float GoalConversionContext;
		/// <summary> Цена достижения цели Яндекс.Метрики при переходе с поиска </summary>
		public float GoalCostSearch;
		/// <summary> Цена достижения цели Яндекс.Метрики при переходе из Рекламной сети Яндекса </summary>
		public float GoalCostContext;
	}
}
