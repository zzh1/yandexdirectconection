﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;

namespace YandexDirectUsing
{
	public class YandexDirectInfo
	{
		/// <summary> Отладачная инфа ответа сервера </summary>
		public string DebugServerAnswer = "non";
		
		
		/// <summary> Имя пользователя </summary>
		public string UserName { get; set; }

		/// <summary> Токен </summary>
		public string Token { get; set; }

		/// <summary> Мастер токен </summary>
		public string MasterToken { get; set; }


		 
		//string jsonUrl = "https://api-sandbox.direct.yandex.ru/v4/json/";
		/// <summary> Адрес для отправки json-запросов </summary>
		private string jsonUrl = "https://api.direct.yandex.ru/v4/json/";

		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Игнорировать сертификат </summary>
		////============================================
		private static bool IgnoreCertificateErrorHandler(object sender,
												   System.Security.Cryptography.X509Certificates.X509Certificate cert,
												   System.Security.Cryptography.X509Certificates.X509Chain chain,
												   SslPolicyErrors sslErr)
		{
			return true;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Конструктор по умолчанию </summary>
		////=============================================
		public YandexDirectInfo()
		{
			
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Конструктор с основными параметрами </summary>
		////========================================================
		public YandexDirectInfo(string userName, string token, string masterToken)
		{
			UserName = userName;
			Token = token;
			MasterToken = masterToken;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить список Id компаний  </summary>
		////=================================================
		public List<int> GetCompanysIdList()
		{
			var companysIdList = new List<int>();
			string retInfo = "";

			if (TestInitParams())
			{
				// Название метода
				string apiMethod = "GetCampaignsList";

				// входная структура
				var getCampaignsList = new
				{
					// авторизационный токен
					token = Token,
					// метод API
					method = apiMethod,
					//param = new string[1] { "zzh1" },
				};

				retInfo = MakeWebRequest(getCampaignsList);

				// Список элементов массива
				string[] mass = retInfo.Split("{".ToCharArray());

				if (mass.Length > 1)
				{
					for (int i = 1; i < mass.Length; i++)
					{
						// Элементы массива
						string[] subMass = mass[i].Split(",".ToCharArray());
						if (subMass.Length > 5)
						{
							// Название поля и значение
							string[] elemMass = subMass[5].Split(":".ToCharArray());
							if (elemMass.Length > 1)
							{
								int elemId = 0;
								int.TryParse(elemMass[1], out elemId);
								companysIdList.Add(elemId);
							}
						}
					}
				}

			}
			else
			{
				
			}

			return companysIdList;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Инфа по компаниям с авто расчётом времени </summary>
		////==============================================================
		public RepositorySummaryStat GetSummaryStatFrom(DateTime upBorderDate)
		{
		
			var retStatInfos = new List<GetSummaryStatInfo>();

			DebugServerAnswer = "";

			// Список id компаний
			int[] companysIds = GetCompanysIdList().ToArray();

			// На самом деле 1000, но возьмём с небольшим запасом
			const int YANDEX_MAX_DAY = 980;
			// На самом деле 1000, но возьмём с небольшим запасом
			const int SERVER_TIME_MINUS = -7;

			DateTime endDate = default(DateTime);
			DateTime startDate = default(DateTime);

			// Если есть компании
			if (companysIds.Length > 0)
			{
				// Кол-во дней, чтобы не выпасть за размер запроса
				int daysMinus = YANDEX_MAX_DAY / companysIds.Length;

				startDate = upBorderDate.AddDays(-daysMinus);
				endDate = upBorderDate.AddHours(SERVER_TIME_MINUS);

				retStatInfos = GetSummaryStat(startDate, endDate, companysIds);
			}

			// Сохранить данные в репазитории
			var retRepositorySummaryStat = new RepositorySummaryStat
			{
				EndTimeBorder = endDate,
				StartTimeBorder = startDate,
				SummaryStat = retStatInfos
			};

			return retRepositorySummaryStat;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Инфа по компаниям с авто расчётом времени </summary>
		////==============================================================
		public List<GetSummaryStatInfo> GetSummaryStat()
		{
			return GetSummaryStatFrom(DateTime.Now).SummaryStat;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Инфа по компаниям c определённым временем </summary>
		////==============================================================
		public List<GetSummaryStatInfo> GetSummaryStat(DateTime startDate, DateTime endDate)
		{
			DebugServerAnswer = "";
			
			// Список id компаний
			int[] companysIds = GetCompanysIdList().ToArray();

			return GetSummaryStat(startDate, endDate, companysIds);
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Инфа по компаниям </summary>
		////======================================
		private List<GetSummaryStatInfo> GetSummaryStat(DateTime startDate, DateTime endDate, int[] companysIds)
		{
			var summaryStat = new List<GetSummaryStatInfo>();

			string retInfo = "";

			// Название метода
			string apiMethod = "GetSummaryStat";
			
			var summaryStatParam = new
			{
				// 
				CampaignIDS = companysIds,
				// 
				StartDate = GetDateToYandexFormat(startDate),
				// 
				EndDate = GetDateToYandexFormat(endDate),
			};

			// входная структура
			var summaryStatInfo = new
			{
				// авторизационный токен
				token = Token,
				// метод API
				method = apiMethod,
				// Параметры запроса
				param = summaryStatParam
			};

			retInfo = MakeWebRequest(summaryStatInfo);

			summaryStat = GetSummaryStatByString(retInfo);

			return summaryStat;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить GetSummaryStatInfo из string значения </summary>
		////===================================================================
		private List<GetSummaryStatInfo> GetSummaryStatByString(string responce)
		{
			var summaryStat = new List<GetSummaryStatInfo>();
			
			// Список элементов массива
			string[] mass = responce.Split("{".ToCharArray());

			if (mass.Length > 1)
			{
				for (int i = 2; i < mass.Length; i++)
				{
					// Элементы массива
					string[] subMass = mass[i].Split(",".ToCharArray());
					if (subMass.Length > 13)
					{
						var elem = new GetSummaryStatInfo();

						foreach (var s in subMass)
						{
							// Название поля и значение
							string[] elemMass = s.Split(":".ToCharArray());

							if (elemMass.Length > 1)
							{
								// По первому елементу заполнить поле
								switch (elemMass[0].Replace("\"",""))
								{
									case "ClicksSearch":
										elem.ClicksSearch = GetIntElemByStr(elemMass[1]);
										break;
									case "SumSearch":
										elem.SumSearch = GetFloatElemByStr(elemMass[1]);
										break;
									case "SessionDepthSearch":
										elem.SessionDepthSearch = GetFloatElemByStr(elemMass[1]);
										break;
									case "SessionDepthContext":
										elem.SessionDepthContext = GetFloatElemByStr(elemMass[1]);
										break;
									case "ClicksContext":
										elem.ClicksContext = GetIntElemByStr(elemMass[1]);
										break;
									case "GoalCostContext":
										elem.GoalCostContext = GetFloatElemByStr(elemMass[1]);
										break;
									case "GoalCostSearch":
										elem.GoalCostSearch = GetFloatElemByStr(elemMass[1]);
										break;
									case "GoalConversionSearch":
										elem.GoalConversionSearch = GetFloatElemByStr(elemMass[1]);
										break;
									case "SumContext":
										elem.SumContext = GetFloatElemByStr(elemMass[1]);
										break;
									case "GoalConversionContext":
										break;
									case "StatDate":
										elem.StatDate = GetDateFromYandexFormat(elemMass[1]);
										break;
									case "ShowsSearch":
										elem.ShowsSearch = GetIntElemByStr(elemMass[1]);
										break;
									case "CampaignID":
										elem.CampaignId = GetIntElemByStr(elemMass[1]);
										break;
									case "ShowsContext":
										elem.ShowsContext = GetIntElemByStr(elemMass[1]);
										break;
								}
							}
						}
						summaryStat.Add(elem);
					}
				}
			}

			return summaryStat;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить int из string значения </summary>
		////====================================================
		private int GetIntElemByStr(string str)
		{
			int digit;
			int.TryParse(str, out digit);

			return digit;
		}

		
		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить float из string значения </summary>
		////======================================================
		private float GetFloatElemByStr(string str)
		{
			str = str.Replace('"', ' ');
			
			float digit;
			float.TryParse(str, out digit);

			return digit;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить дату в формате Yandex`а </summary>
		////=====================================================
		private string GetDateToYandexFormat(DateTime dateTime)
		{
			return dateTime.ToString("yyyy-MM-dd");
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить дату из формата Yandex`а </summary>
		////======================================================
		private DateTime GetDateFromYandexFormat(string dateTime)
		{
			dateTime = dateTime.Replace('"', ' ');
			
			var setDate = new DateTime();
			string[] mass = dateTime.Split("-".ToCharArray());
			if (mass.Length > 2)
			{
				int year, month, day;
				int.TryParse(mass[0], out year);
				int.TryParse(mass[1], out month);
				int.TryParse(mass[2], out day);

				
				setDate = new DateTime(year,month,day);
			}

			return setDate;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Произвести Web запрос </summary>
		////==========================================
		private string MakeWebRequest(object structInfo)
		{
			// сериализуем объект CreditLimitsInfo в формат нотации JSON
			var jss = new JavaScriptSerializer();
			string json = jss.Serialize(structInfo);

			// SSL-сертификат не используется
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);

			// создаем клиента
			var wc = new WebClient();

			// отправляем POST-запрос и получаем ответ
			byte[] result = wc.UploadData(jsonUrl, "POST", Encoding.UTF8.GetBytes(json));

			string retInfo = Encoding.UTF8.GetString(result);

			DebugServerAnswer += "\r\n================\r\n" + retInfo;

			return retInfo;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Проверка параметров инициализации </summary>
		////======================================================
		private bool TestInitParams()
		{
			bool isInitParams = false;

			if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(MasterToken))
			{
				isInitParams = true;
			}

			return isInitParams;
		}


		//______________________________________________________________________________________________________________________________________________________________________________________
		/// <summary> Получить финансовый токен </summary>
		////==============================================
		private string GetFinanceToken(string operationNumber, string apiMethod)
		{
			// генерация финансового токена
			SHA256Managed sha = new SHA256Managed();
			string fullData = MasterToken + operationNumber + apiMethod + UserName;
			byte[] fullCach = sha.ComputeHash(Encoding.UTF8.GetBytes(fullData));
			string financeToken = "";
			foreach (byte b in fullCach)
			{
				financeToken += String.Format("{0:x2}", b);
			}

			return financeToken;
		}
	}
}
